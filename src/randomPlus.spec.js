const randomPlus = require('./randomPlus');
const random = require('./random');

jest.mock('./random');

random.mockReturnValueOnce(1);
random.mockReturnValueOnce(99);

describe('randomPlus', function() {
  it ('99 + 1 (random) = 100', function() {
    expect(randomPlus(99)).toBe(100);
  });
  it ('1 + 99 (random) = 100', function() {
    expect(randomPlus(1)).toBe(100);
});
});

