const user = require('./user');

describe('user', function() {
  it('Input is not a string', function() {
    expect(function() {
      user('person'); 
    }).toThrow('id needs to be integer');
  });
});